\documentclass[10pt, a4paper, abstract=true, bibliography = totocnumbered]{scrartcl}

\usepackage{packages}
\usepackage{latex-commands/commands}

\title{Algebraic Group actions}
\author{Manuel Hoff}
\date{}

\begin{document}

    \maketitle

    Fix an algebraically closed field $k$.

    \begin{notation}
        We use the following notation:
        \begin{itemize}
            \item
            The terms \enquote{algebraic group/scheme} and \enquote{algebra} mean \enquote{$k$-(group) scheme of finite type} and \enquote{finitely generated $k$-algebra}.

            \item
            We denote by $\catalgs$ the category of algebras.

            \item
            We will not distinguish between an algebraic scheme and its associated functor $\catalgs \to \catsets$.
        \end{itemize}
    \end{notation}

    \section{Recollection of (set-theoretic) group actions}

    Let $G$ be an (abstract) group.

    \begin{definition}
        Let $X$ be a set.
        A \emph{group action of $G$ on $X$} is a map of sets
        \[
            G \times X \to X, \qquad (g, x) \mapsto g.x
        \]
        that satisfies the following properties:
        \begin{itemize}
            \item
            $1.x = x$ for all $x \in X$.

            \item
            $(gh).x = g.(h.x)$ for all $g, h \in G$ and $x \in X$.
        \end{itemize}
        Giving a group action of $G$ on $X$ is equivalent to giving a map of groups $G \to S_X$ where $S_X$ denotes the symmetric group on the set $X$.
        A set together with a group action by $G$ is also called a \emph{$G$-set}.
    \end{definition}

    \begin{definition}
        Let $X$ be a $G$-set and let $x \in X$.
        Associated to $x$ we have the following data:
        \begin{itemize}
            \item
            The subgroup
            \[
                \Stab_G(x) \coloneqq \set[\big]{g \in G}{g.x = x} \subseteq G
            \]
            is called the \emph{stabilizer of $x$ in $G$}.

            \item
            The subset
            \[
                G.x \coloneqq \set[\big]{g.x}{g \in G} \subseteq X
            \]
            is called the \emph{orbit of $x$ under the action of $G$}.
        \end{itemize}
    \end{definition}

    We have the following elementary fact:

    \begin{lemma}
        Let $X$ be a $G$-set and let $x \in X$.
        Then the map
        \[
            G/\Stab_G(x) \to G.x, \qquad g \Stab_G(x) \mapsto g.x
        \]
        is a well-defined bijection.
    \end{lemma}

    \section{Algebraic group actions}
    
    Let $G$ be an algebraic group.

    \begin{definition}
        Let $X$ be an algebraic scheme.
        An \emph{algebraic group action of $G$ on $X$} is a map of algebraic schemes
        \[
            G \times X \to X, \qquad (g, x) \mapsto g.x
        \]
        such that for every $R \in \catalgs$ the induced map on points $G(R) \times X(R) \to X(R)$ is an action of the (abstract) group $G(R)$ on the set $X(R)$.
        An algebraic scheme together with an algebraic group action by $G$ is also called an \emph{algebraic $G$-scheme}.
    \end{definition}

    The goal is now to define stabilizers and orbits for algebraic group actions.
    For the rest of the section, fix an algebraic $G$-scheme $X$ and a $k$-valued point $x \in X(k)$ and denote the action map $G \to X, \, g \mapsto g.x$ by $a_x$.

    \subsection{Stabilizers}

    \begin{definition}
        The \emph{stabilizer of $x$ in $G$} is the subfunctor (of groups) $\Stab_G(x) \subseteq G$ that is defined by
        \[
            \Stab_G(x)(R) \coloneqq \Stab_{G(R)}(x) \subseteq G(R)
        \]
        for all $R \in \catalgs$.
        Note that we (maybe confusingly) use the same notation for $x$ and its image in $X(R)$.
    \end{definition}

    \begin{lemma}
        $\Stab_G(x) \subseteq G$ is a (closed) algebraic subgroup.
    \end{lemma}

    \begin{proof}
        We have a pullback diagram of functors $\catalgs \to \catsets$
        \[
        \begin{tikzcd}
            \Stab_G(x) \arrow{r} \arrow{d}
            &G \arrow{d}{a_x}
            \\
            \Spec(k) \arrow{r}{x}
            &X
        \end{tikzcd}
        \]
        implying that $\Stab_G(x)$ is an algebraic scheme as desired.
    \end{proof}

    \subsection{Orbits}

    We now also want to define orbits for algebraic group actions.
    Before we do this we need one definition and one theorem that we treat as a blackbox.

    \begin{definition}
        A map of algebraic schemes $f \colon X \to Y$ is called \emph{surjective} if the induced map on $k$-valued points $X(k) \to Y(k)$ is surjective (or equivalently if the underlying map of topological spaces $\abs{X} \to \abs{Y}$ is surjective).
    \end{definition}

    \begin{theorem}[Generic flatness]
        Let $f \colon X \to Y$ be a dominant map of algebraic schemes.
        Then there exists a (set-theoretically) dense open subscheme $U \subseteq Y$ such that the induced map $f^{-1}(U) \to U$ is surjective.
    \end{theorem}

    Now we can state and prove our main result.

    \begin{lemma}
        Suppose that $G$ is smooth.
        Then there exists a unique locally closed subscheme $G.x \subseteq X$ with the following properties:
        \begin{itemize}
            \item
            The map $a_x \colon G \to X$ factors over $G.x$.
            
            \item
            The resulting map $a_x \colon G \to G.x$ is surjective (in the sense that it is surjective on $k$-valued points).

            \item
            $G.x$ is reduced.
        \end{itemize}
    \end{lemma}

    \begin{proof}
        Let $Z \subseteq X$ be the closure of the image of the map of topological spaces $\abs{a_x} \colon \abs{G} \to \abs{X}$, equipped with the reduced subscheme structure.
        As $G$ is reduced the map $a_x$ factors over $Z \subseteq X$ and the resulting map $a_x \colon G \to Z$ is dominant.

        We now claim that $Z$ is stable under the $G$-action on $X$, i.e.\ that the action map $G \times X \to X$ restricts to a map $G \times Z \to Z$.
        As $G \times Z$ is reduced it suffices to \enquote{show this on $k$-valued points} (meaning it suffices to show that $g.z \in Z(k)$ for all $g \in G(k)$ and $z \in Z(k)$).
        To see this, fix $g \in G(k)$ and consider the commutative diagram
        \[
        \begin{tikzcd}
            G \arrow{r}{a_x} \arrow{d}{g}
            &X \arrow{d}{g}
            \\
            G \arrow{r}{a_x}
            &X
        \end{tikzcd}
        \]
        where we note that the vertical maps are isomorphisms.
        As $Z$ is defined as the closure of the (topological) image of either of the horizontal maps, this implies that $g.Z = Z$.

        Now we apply the generic flatness theorem to $a_x \colon G \to Z$ to obtain an open subscheme $U \subseteq Z$ such that the map $a_x \colon a_x^{-1}(U) \to U$ is surjective.
        
        Now, for every $g \in G(k)$, we have a commutative diagram
        \[
        \begin{tikzcd}
            a_x^{-1}(U) \arrow{r}{a_x} \arrow{d}{g}
            &U \arrow{d}{g}
            \\
            g \cdot a_x^{-1}(U) = a_x^{-1}(g.U) \arrow{r}{a_x}
            &g.U
        \end{tikzcd}
        \]
        where again the vertical maps are isomorphisms.
        Thus also the map $a_x \colon g \cdot \alpha^{-1}(U) \to g.U$ is surjective.
        Taking the union over all $g$ we see that also
        \[
            a_x \colon G = \bigcup_{g \in G(k)} g \cdot a_x^{-1}(U) \to \bigcup_{g \in G(k)} g.U \eqqcolon V
        \]
        is surjective.

        Thus we see that we can take $G.x \coloneqq V$ (this is a locally closed subscheme of $X$ because it is an open subscheme of $Z$ and $Z$ is a closed subscheme of $X$).
        The uniqueness assertion is clear because the reduced locally closed subscheme $G.x \subseteq X$ is determined by its set of $k$-valued points $(G.x)(k)$ which is determined by the second assumption.
    \end{proof}

    \begin{remark}
        We make the following remarks.
        \begin{itemize}
            \item
            When $G$ is not smooth then the map $a_x \colon G \to X$ may fail to factor over $Z$.
            However there still exists a sensible definition of orbit, but this orbit may not be reduced anymore.
            
            \item
            $G.x$ is always equidimensional (because $G$ acts on it and this action is transitive on $k$-rational points).
        \end{itemize}
    \end{remark}

    \begin{lemma}[Closed orbit Lemma]
        Suppose that $G.x$ is of minimal dimension among the $G.y$ for $y \in X(k)$.
        Then $G.x \subseteq X$ is a closed subscheme.
    \end{lemma}

    \begin{proof}
        Let $Z \subseteq X$ be defined as in the proof of the previous Lemma.
        We need to show that $G.x = Z$. So suppose that this is not the case.

        Then there exists $y \in Z(k) \setminus (G.x)(k)$. The orbit $G.y \subseteq Z$ is disjoint from $G.x$, implying that $\dim G.y < \dim G.x$ and yielding a contradiction.
    \end{proof}

    \subsection{Examples}

    Let's end with two examples:

    \begin{example}
        Let $G \coloneqq \GL_2$ and $X = \projspace^1$ (so that $\projspace^1(R)$ is the set of all rank 1 direct summands $L$ of $R^2$).
        Then there is a natural action of $G$ on $X$ (elements in $\GL_2(R)$ can be considered as automorphisms of $R^2$).

        This action has only one orbit and the stabilizer of the representative $[1, 0] \in \projspace^1(k)$ is given by
        \[
            \Stab_G\roundbr[\big]{[1, 0]}(R) = \set[\Big]{\begin{psmallmatrix}a &b \\ c &d\end{psmallmatrix}}{c = 0}.
        \]
    \end{example}

    \begin{example}
        Suppose that $k$ is of positive characteristic $p$.
        Let $G \coloneqq \mu_p$ and $X \coloneqq \affspace^1$ and define an action of $G$ on $X$ by
        \[
            g.x \coloneqq g \cdot x.
        \]
        Let $x \coloneqq 1 \in \affspace^1(k)$.
        Then the topological image of the action map $a_x \colon G \to X$ is just given by the single point $x$ again (or more precisely the closed point of $\abs{X}$ that corresponds to $x$).
        But the action map $a_x \colon G \to X$ does not factor over $V(t - 1) \subseteq X$.

        To see this, consider $R \coloneqq k[\varepsilon]/(\varepsilon^2) \in \catalgs$ and the $R$-valued point $g \coloneqq 1 + \varepsilon \in G(R)$.
        Then we have
        \[
            a_x(g) = (1 + \varepsilon) \cdot 1 = 1 + \varepsilon \notin V(t - 1)(R).
        \]
    \end{example}

\end{document}

