\documentclass[10pt, a4paper, abstract=true, bibliography = totocnumbered]{scrartcl}

\usepackage{packages}
\usepackage{latex-commands/commands}

\title{Algebraic Group actions}
\author{Manuel Hoff}
\date{}

\begin{document}

    \maketitle

    Fix an algebraically closed field $k$.

    \begin{notation}
        We use the following notation:
        \begin{itemize}
            \item
            The terms \enquote{algebraic group/scheme} and \enquote{algebra} mean \enquote{$k$-(group) scheme of finite type} and \enquote{finitely generated $k$-algebra}.

            \item
            We denote by $\catalgs$ the category of algebras.

            \item
            We turn $\catalgs^{\op}$ into a site by equipping it with the flat topology.
            A generating class of coverings is given by all finite families $(R \to R_i)_{i \in I}$ of maps in $\catalgs$ such that the induced map $R \to \prod_{i \in I} R_i$ is faithfully flat.

            \item
            We will not distinguish between an algebraic scheme and its associated sheaf on $\catalgs^{\op}$.
        \end{itemize}
    \end{notation}

    \section{Recollection of (set-theoretic) group actions}

    Let $G$ be an (abstract) group.

    \begin{definition}
        Let $X$ be a set.
        A \emph{group action of $G$ on $X$} is a map of sets
        \[
            G \times X \to X, \qquad (g, x) \mapsto g.x
        \]
        that satisfies the following properties:
        \begin{itemize}
            \item
            $1.x = x$ for all $x \in X$.

            \item
            $(gh).x = g.(h.x)$ for all $g, h \in G$ and $x \in X$.
        \end{itemize}
        Giving a group action of $G$ on $X$ is equivalent to giving a map of groups $G \to S_X$ where $S_X$ denotes the symmetric group on the set $X$.
        A set together with a group action by $G$ is also called a \emph{$G$-set}.
    \end{definition}

    \begin{definition}
        Let $X$ be a $G$-set and let $x \in X$.
        Associated to $x$ we have the following data:
        \begin{itemize}
            \item
            The subgroup
            \[
                \Stab_G(x) \coloneqq \set[\big]{g \in G}{g.x = x} \subseteq G
            \]
            is called the \emph{stabilizer of $x$ in $G$}.

            \item
            The subset
            \[
                G.x \coloneqq \set[\big]{g.x}{g \in G} \subseteq X
            \]
            is called the \emph{orbit of $x$ under the action of $G$}.
        \end{itemize}
    \end{definition}

    We have the following elementary fact:

    \begin{lemma}
        Let $X$ be a $G$-set and let $x \in X$.
        Then the map
        \[
            G/\Stab_G(x) \to G.x, \qquad g \Stab_G(x) \mapsto g.x
        \]
        is a well-defined bijection.
    \end{lemma}

    \section{Algebraic group actions}
    
    Let $G$ be an algebraic group.

    \begin{definition}
        Let $X$ be an algebraic scheme.
        An \emph{algebraic group action of $G$ on $X$} is a map of algebraic schemes
        \[
            G \times X \to X, \qquad (g, x) \mapsto g.x
        \]
        such that for every $R \in \catalgs$ the induced map on points $G(R) \times X(R) \to X(R)$ is an action of the (abstract) group $G(R)$ on the set $X(R)$.
        An algebraic scheme together with an algebraic group action by $G$ is also called an \emph{algebraic $G$-scheme}.
    \end{definition}

    The goal is now to define stabilizers and orbits for algebraic group actions.
    For the rest of the section, fix an algebraic $G$-scheme $X$ and a $k$-rational point $x \in X(k)$ and denote the action map $G \to X, \, g \mapsto g.x$ by $a_x$.

    \subsection{Stabilizers}

    \begin{definition}
        The \emph{stabilizer of $x$ in $G$} is the subpresheaf (of groups) $\Stab_G(x) \subseteq G$ that is defined by
        \[
            \Stab_G(x)(R) \coloneqq \Stab_{G(R)}(x) \subseteq G(R)
        \]
        for all $R \in \catalgs$.
        Note that we (maybe confusingly) use the same notation for $x$ and its image in $X(R)$.
    \end{definition}

    \begin{lemma}
        $\Stab_G(x) \subseteq G$ is a (closed) algebraic subgroup.
    \end{lemma}

    \begin{proof}
        We have a pullback diagram
        \[
        \begin{tikzcd}
            \Stab_G(x) \arrow{r} \arrow{d}
            &G \arrow{d}{a_x}
            \\
            \Spec(k) \arrow{r}{x}
            &X
        \end{tikzcd}
        \]
        implying that $\Stab_G(x)$ is an algebraic scheme as desired.
    \end{proof}

    \subsection{Orbits}

    \begin{definition}
        The \emph{orbit of $x$ under the action of $G$} is the subsheaf $G.x \subseteq X$ that is given as the (sheaf-theoretic) image of the map $a_x \colon G \to X$.

        More concretely, for an algebra $R$, the subset $(G.x)(R) \subseteq X(R)$ consists of all $y \in X(R)$ such that there exists a faithfully flat map of algebras $R \to R'$ and a group element $g \in G(R')$ such that $g.x = y$ in $X(R')$.
    \end{definition}

    With this definition the following result is formal:

    \begin{lemma}
        The map $a_x \colon G \to G.x$ induces an isomorphism $G/\Stab_G(x) \to G.x$ (where the quotient is the sheaf quotient).
    \end{lemma}

    We now want to show that the definition of $G.x$ is well-behaved.
    To do this we will need the generic flatness theorem:

    \begin{theorem}[Generic flatness]
        Let $f \colon X \to Y$ be a map of algebraic schemes and assume that $Y$ is reduced.
        Then there exists a (set-theoretically) dense open subscheme $U \subseteq Y$ such that the induced map $f^{-1}(U) \to U$ is flat.
    \end{theorem}

    \begin{lemma}
        Suppose that $G$ is smooth.
        Then $G.x \subseteq X$ is a locally closed subscheme and the map $a_x \colon G \to G.x$ is faithfully flat.
    \end{lemma}

    \begin{proof}
        Let $Z \subseteq X$ be the scheme-theoretic image of $a_x$ (or equivalently the closure of the image of the underlying map of topological spaces equipped with the reduced subscheme structure).
        Then $Z$ is stable under the $G$-action on $X$ and the map $a_x \colon G \to Z$ is dominant.

        As $Z$ is reduced we may apply the generic flatness theorem to obtain a dense open subscheme $U \subseteq Z$ such that $a_x \colon a_x^{-1}(U) \to U$ is flat.
        As $a_x$ is dominant (when corestricted to $Z$) we see that $a_x^{-1}(U) \neq \emptyset$.
        Shrinking $U$ we thus may even assume that $a_x \colon a_x^{-1}(U) \to U$ is faithfully flat.
        
        For every $g \in G(k)$ we have a commutative square
        \[
        \begin{tikzcd}
            a_x^{-1}(U) \arrow{r}{a_x} \arrow{d}{g}
            &U \arrow{d}{g}
            \\
            g \cdot a_x^{-1}(U) = a_x^{-1}(g.U) \arrow{r}{a_x}
            &g.U
        \end{tikzcd}
        \]
        where the vertical maps are isomorphisms.
        Thus also the map $a_x \colon g \cdot \alpha^{-1}(U) \to g.U$ is faithfully flat.
        Taking the union over all $g$ we see that also
        \[
            a_x \colon G = \bigcup_{g \in G(k)} g \cdot a_x^{-1}(U) \to \bigcup_{g \in G(k)} g.U \eqqcolon V
        \]
        is faithfully flat.
        As a faithfully flat map of algebraic schemes is in particular surjective as a map of sheaves, we have $V = G.x$.
        This proves the claim ($V$ is an open subscheme of $Z$ and $Z$ is a closed subscheme of $X$).
    \end{proof}

    \begin{remark}
        We make the following remarks.
        \begin{itemize}
            \item
            Without the smoothness assumption on $G$ the above proof doesn't work anymore because $Z$ may fail to be reduced.
            However the claim of the Lemma is still true (but to prove this requires more work).
            
            \item
            $G.x$ is always equidimensional (because $G$ acts on it and this action is transitive on $k$-rational points).
            If $G$ is smooth then also $G.x$ is smooth (because $a_x \colon G \to G.x$ is faithfully flat).
        \end{itemize}
    \end{remark}

    \begin{lemma}[Closed orbit Lemma]
        Suppose that $G.x$ is of minimal dimension among the $G.y$ for $y \in X(k)$.
        Then $G.x \subseteq X$ is a closed subscheme.
    \end{lemma}

    \begin{proof}
        As before, let $Z \subseteq X$ be the scheme-theoretic image of $a_x$.
        We need to show that $G.x = Z$. So suppose that this is not the case.

        Then there exists $y \in Z(k) \setminus (G.x)(k)$. The orbit $G.y \subseteq Z$ is disjoint from $G.x$, implying that $\dim G.y < \dim G.x$ and yielding a contradiction.
    \end{proof}

    \subsection{Examples}

    Let's end with two examples:

    \begin{example}
        Suppose that $k$ is of positive characteristic $p$.
        Let $G$ be the algebraic group defined by
        \[
            G(R) \coloneqq \set[\big]{(g, t) \in R^{\times} \times R}{t^p = 0}
        \]
        with group structure
        \[
            (g, t) \cdot (g', t') \coloneqq (gg', gt' + t).
        \]
        In other words, $G$ is a semidirect product of $\Gm$ and $\alpha_p$ (for the action of $\Gm$ on $\alpha_p$ given by multiplication).
        Let $X \coloneqq \affspace^1$ and define an action of $G$ on $X$ by
        \[
            (g, t).x \coloneqq g x + t.
        \]
        One can verify that this is indeed a group action.
        There are the following two orbits:
        \begin{itemize}
            \item
            One orbit is given by $D(x) \subseteq \affspace^1$.
            The stabilizer of the representative $1 \in D(x)(k)$ is given by
            \[
                \Stab_G(1)(R) = \set[\big]{(g, t)}{g + t = 1}
            \]
            and is isomorphic to $\mu_p$ via the map
            \[
                \mu_p \to \Stab_G(1), \qquad g \mapsto (g, 1 - g).
            \]

            \item
            The other orbit is given by $V(x^p) \subseteq \affspace^1$ (and is not reduced).
            The stabilizer of its only point $0 \in D(x)(k)$ is given by
            \[
                \Stab_G(0)(R) = \set[\big]{(g, t)}{t = 0}
            \]
            and is clearly isomorphic to $\Gm$.
        \end{itemize}
    \end{example}

    \begin{example}
        Let $G \coloneqq \GL_2$ and $X = \projspace^1$ (so that $\projspace^1(R)$ is the set of all rank 1 direct summands $L$ of $R^2$).
        Then there is a natural action of $G$ on $X$ (elements in $\GL_2(R)$ can be considered as automorphisms of $R^2$).

        This action has only one orbit and the stabilizer of the representative $[1, 0] \in \projspace^1(k)$ is given by
        \[
            \Stab_G\roundbr[\big]{[1, 0]}(R) = \set[\Big]{\begin{psmallmatrix}a &b \\ c &d\end{psmallmatrix}}{c = 0}.
        \]
    \end{example}
\end{document}

